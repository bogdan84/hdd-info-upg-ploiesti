#include "HardDiskManager.h"

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <memory>
#include <string>

using namespace std;

HardDiskManager::HardDiskManager()
{
	m_uliFreeBytesAvailable.QuadPart     = 0L;
	m_uliTotalNumberOfBytes.QuadPart     = 0L;
	m_uliTotalNumberOfFreeBytes.QuadPart = 0L;
}

HardDiskManager::~HardDiskManager(){}

bool HardDiskManager::CheckFreeSpace(LPCTSTR lpDirectoryName)
{
	if( !GetDiskFreeSpaceEx(
		lpDirectoryName,
		&m_uliFreeBytesAvailable,
		&m_uliTotalNumberOfBytes,
		&m_uliTotalNumberOfFreeBytes) )
		return false;

	return true;
}

DWORD64 HardDiskManager::GetTotalNumberOfBytes(void)
{ 
	return m_uliTotalNumberOfBytes.QuadPart;
}

DWORD64 HardDiskManager::GetTotalNumberOfFreeBytes(void)
{ 
	return m_uliTotalNumberOfFreeBytes.QuadPart;
}

double HardDiskManager::GetTotalNumberOfGBytes(void)
{ 
	return (double)( (signed __int64)(m_uliTotalNumberOfBytes.QuadPart)/1.0e9 );     
}

double HardDiskManager::GetTotalNumberOfFreeGBytes(void)
{ 
	return (double)( (signed __int64)(m_uliTotalNumberOfFreeBytes.QuadPart)/1.0e9 ); 
}

void HardDiskManager::GetDrives(void)
{
	int dr_type=99;
	int MAX = 256;
	char dr_avail[MAX];
	char *temp=dr_avail;

	GetLogicalDriveStrings(MAX,dr_avail);

	while(*temp != '\0') {
		dr_type=GetDriveType(temp);

		switch(dr_type) {
			case 0: // Unknown
				cout << temp << " Unknown Drive type" << endl;
				break;
			case 1: // Invalid
				cout << temp << " Drive is invalid" << endl;
				break;
			case 2: // Removable Drive
				cout << temp << " Removable Drive" << endl;
				break;
			case 3: // Fixed
				this->CheckFreeSpace(temp);
				cout << temp << " Hard Disk (Fixed)" << endl;
				cout << "Spatiu disc total:     " << this->GetTotalNumberOfGBytes()     << " GBytes " << endl;
				cout << "Spatiu disc liber:     " << this->GetTotalNumberOfFreeGBytes()     << " GBytes " << endl;
				cout << "Spatiu disc ocupat:    " << this->GetTotalNumberOfGBytes() - this->GetTotalNumberOfFreeGBytes() << " GBytes \n" << endl;
				break;
			case 4: // Remote
				cout << temp << " Remote (Network) Drive" << endl;
				break;
			case 5: // CDROM
				cout << temp << " CD-Rom/DVD-Rom" << endl;
				break;
			case 6: // RamDrive
				cout << temp << " Ram Drive" << endl;
				break;
			default:
				cout << temp << " Unknown Drive type" << endl;
		}
		temp += lstrlen(temp) +1;
	}
}

const char* HardDiskManager::GetHddInfo(void)
{
	//get a handle to the first physical drive
	HANDLE h = CreateFileW(L"\\\\.\\PhysicalDrive0", 0, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, NULL);
	if (h == INVALID_HANDLE_VALUE) return {};
	//an std::unique_ptr is used to perform cleanup automatically when returning (i.e. to avoid code duplication)
	unique_ptr<remove_pointer<HANDLE>::type, void (*)(HANDLE)> hDevice{h, [](HANDLE handle) {
		CloseHandle(handle);
	}};
	//initialize a STORAGE_PROPERTY_QUERY data structure (to be used as input to DeviceIoControl)
	STORAGE_PROPERTY_QUERY storagePropertyQuery{};
	storagePropertyQuery.PropertyId = StorageDeviceProperty;
	storagePropertyQuery.QueryType = PropertyStandardQuery;
	//initialize a STORAGE_DESCRIPTOR_HEADER data structure (to be used as output from DeviceIoControl)
	STORAGE_DESCRIPTOR_HEADER storageDescriptorHeader{};
	//the next call to DeviceIoControl retrieves necessary size (in order to allocate a suitable buffer)
	//call DeviceIoControl and return an empty std::string on failure
	DWORD dwBytesReturned = 0;
	if (!DeviceIoControl(hDevice.get(), IOCTL_STORAGE_QUERY_PROPERTY, &storagePropertyQuery,
						 sizeof(STORAGE_PROPERTY_QUERY),
						 &storageDescriptorHeader, sizeof(STORAGE_DESCRIPTOR_HEADER), &dwBytesReturned, NULL))
		return {};
	//allocate a suitable buffer
	const DWORD dwOutBufferSize = storageDescriptorHeader.Size;
	unique_ptr<BYTE[]> pOutBuffer{new BYTE[dwOutBufferSize]{}};
	//call DeviceIoControl with the allocated buffer
	if (!DeviceIoControl(hDevice.get(), IOCTL_STORAGE_QUERY_PROPERTY, &storagePropertyQuery,
						 sizeof(STORAGE_PROPERTY_QUERY),
						 pOutBuffer.get(), dwOutBufferSize, &dwBytesReturned, NULL))
		return {};
	//read and return the serial number out of the output buffer
	STORAGE_DEVICE_DESCRIPTOR *pDeviceDescriptor = reinterpret_cast<STORAGE_DEVICE_DESCRIPTOR *>(pOutBuffer.get());
	const DWORD dwSerialNumberOffset = pDeviceDescriptor->SerialNumberOffset;
	if (dwSerialNumberOffset == 0) return {};
	const char *serialNumber = reinterpret_cast<const char *>(pOutBuffer.get() + dwSerialNumberOffset);

	return serialNumber;
}

void HardDiskManager::GetHddSerialNo()
{
	string serialNumber = this->GetHddInfo();

	if (serialNumber.empty())
		cout << "Eroare la citirea serial number a HDD" << "\n" << endl;
	else
		cout << "HDD serial number: " << serialNumber << "\n" << endl;
}
