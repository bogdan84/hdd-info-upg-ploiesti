#include <windows.h>

class HardDiskManager  
{
	private:
		ULARGE_INTEGER m_uliFreeBytesAvailable;
		ULARGE_INTEGER m_uliTotalNumberOfBytes;
		ULARGE_INTEGER m_uliTotalNumberOfFreeBytes;

		DWORD64 GetTotalNumberOfBytes(void);
		DWORD64 GetTotalNumberOfFreeBytes(void);

		const char* GetHddInfo(void);

	public:
		HardDiskManager();
		virtual ~HardDiskManager();

		bool CheckFreeSpace(LPCTSTR lpDirectoryName);

		double GetTotalNumberOfGBytes(void);
		double GetTotalNumberOfFreeGBytes(void);

		void GetDrives(void);
		void GetHddSerialNo(void);
};

